#include<iostream>
using namespace std;

#define Q_SIZE 500
#define ERROR -99999

class Queue{
private:
	char q[Q_SIZE];
	int front,rear;
public:
	/* Hàm khởi tạo, khi mới khởi tạo queue rỗng
	   front = rear = 0 */
	Queue(){
		front = 0;
		rear = 0;
	}
	/* Hàm chèn ký tự c vào hàng đợi q */
	void Enqueue(char* c);

	/* Hàm lấy phần tử ở đầu ra khỏi hàng đợi */
	char Dequeue();

	/* Hàm trả về giá trị của phần tử đầu tiên trong hàng đợi */
	char getFront();

	/* Hàm kiểm tra xem hàng đợi có rỗng hay không */
	bool isEmpty();

	/* Hàm kiểm tra xem hàng đợi đã đầy hay chưa */
	bool isFull();

	/* Hàm in ra tất cả các phần tử trong hàng đợi */
	void PrintQ();
};

void Queue::Enqueue(char* c){
	/* Kiểm tra xem hàng đợi đã đầy hay chưa,
	   Chỉ enqueue khi hàng đợi chưa đầy */
	if(isFull()){
		cout << "ERROR: Queue is full, can't enqueue" << endl;
		return;
	}
	q[rear++] = *c;
}

char Queue::Dequeue(){
	char temp;
	/* Kiển tra xem hàng đợi có rỗng không,
	   Chỉ Dequeue khi hàng đợi không rỗng */
	if(isEmpty()){
		cout << "ERROR: Queue is empty, can't dequeue" << endl;
		return ERROR;
	}
	temp = q[front++];
	return temp;
}

char Queue::getFront(){
	/* Kiển tra xem hàng đợi có rỗng không,
	   Nếu rỗng trả về lỗi */
	if(isEmpty()){
		cout << "ERROR: Queue is empty, can't get front element" << endl;
		return ERROR;
	}
	return q[front];
}

bool Queue::isEmpty(){
	return (front == 0 && rear == 0);
}

bool Queue::isFull(){
	return (rear == Q_SIZE);
}

void Queue::PrintQ(){
	for(int i = front; i < rear; i++){
		cout << q[i] << " ";
	}
	cout << endl;
}

int main()
{
    /* Hàm kiểm tra hoạt động của toàn bộ chương trinh
       In kết quả sau mỗi bước thực hiện */
   Queue Q; /* Tạo một thể hiện của Queue */
   Q.Enqueue("A");  Q.PrintQ();  
   Q.Enqueue("B");  Q.PrintQ();  
   Q.Enqueue("C");  Q.PrintQ();
   Q.Dequeue();	  Q.PrintQ();
   Q.Dequeue();	  Q.PrintQ();
   Q.Enqueue("D");  Q.PrintQ();
   Q.Enqueue("E");  Q.PrintQ();

   return 0;
}