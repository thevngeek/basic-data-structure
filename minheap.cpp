#include <iostream> 
using namespace std;

int N = 7; /* Số phần tử của heap */
void min_heap(int A[], int i) {
    int smallest; /* Chỉ số của phần tử nhỏ nhất trong bộ ba: node hiện tại
                         con bên trái, và con bên phải của nó */
    int left = 2 * i; /* Vị trí của con bên trái */
    int right = 2 * i + 1; /* Vị trí của con bên phải */
    if (left <= N and A[left] < A[i]) /* N là số phần tử trong mảng, biến toàn cục */
        smallest = left;
    else
        smallest = i;
    if (right <= N and A[right] < A[smallest])
        smallest = right;
    if (smallest != i) {
        swap(A[i], A[smallest]); /* Thực hiện đổi chỗ hai phần tử nếu 
                                              giá trị của node cha lớn hơn node con */
        min_heap(A, smallest); /* Gọi đệ quy node tại vị trí mới */
    }
}

void run_minheap(int A[]) { /* Áp dụng hàm min_heap cho tất cả các node
                                               trừ node lá */
    for (int i = N / 2; i >= 1; i--) {
        min_heap(A, i);
    }
}

int main() {
    int A[N+1] = {-1,10,8,9,7,6,5,4}; /* Mảng chứa 8 phần tử, với phần tử 
                                                       của heap bắt đầu từ vị trí 1 tới N
                                                       phần tử dầu tiên của mảng chỉ có tác
                                                       dụng làm lấp đầy mảng, giá trị bất kỳ */
    run_minheap(A);
    printf("\n\tGiá trị của mảng sau khi áp dụng hàm min_heap\n");
    printf("\t");
    for (int i = 0; i < N+1; i++) { /* In ra giá trị của mảng sau khi sắp xếp với 
                                                 hàm min_heap */
        printf("%d    ", A[i]);
    }

    return 0;
}
