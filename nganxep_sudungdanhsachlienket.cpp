#include <iostream>

using namespace std;

#define INT_MIN -2147483648
#define INT_MAX 2147483648


/* Cấu trúc để chứa danh sách liên kết */
struct sNode {
    int data;
    struct sNode* next;
};

class Stack {
public:
    /* Node root có thuộc tính public để có thể được sửa đổi và tác động 
        từ bên ngoài */
    struct sNode* root;
    
    /* Khởi tạo stack rỗng */
    Stack()
    {
        root = NULL;
    }
    
    /* Hàm để tạo ra một node mới tham tham số truyền 
        vào là một số nguyên */
    struct sNode* newNode(int data)
    {
        struct sNode* s_node = (struct sNode*)malloc(sizeof(struct sNode));
        s_node->data = data;
        s_node->next = NULL;
        return s_node;
    }
    
    /* 
         Thêm dữ liệu vào đỉnh ngăn xếp,
         Đỉnh của ngăn xếp chính là node gốc của danh sách sách liên kết,
         việc chèn phần tủ vào đầu danh sách liên kết thay vì chèn vào cuối danh 
         sách tránh được việc phải duyệt qua toàn bộ danh sách rồi mới chèn,
         chèn ở đầu danh sách có độ phức tạp thời gian là O(1)
    */
    void Push(int data)
    {
        /* Không cần kiểm tra điều kiện tràn ngăn xếp vì ta sử dụng danh sách
            liên kết (cấp phát động) */
        struct sNode* s_node = newNode(data);
        s_node->next = root;
        root = s_node;
    }
    
    /* Xóa phần tử từ đỉnh ngăn xếp (xóa đi phần tử đầu tiên trong danh sách
        liên kết */
    void Pop()
    {
        if (isEmpty()) {
            printf("ERROR: Stack is empty\n");
            return;
        }
        struct sNode* temp = root;
        root = root->next;
        int popped = temp->data;
        free(temp);
    }
    
    /* Trả về giá trị của phần tử đỉnh ngăn xếp */
    int Top()
    {
        if (isEmpty()) {
            printf("ERROR: Stack is empty\n");
            return INT_MIN;
        }
        return root->data;
    }
    
    /* Nếu ngăn xếp rỗng, trả về true. Ngược lại trả về false */
    int isEmpty()
    {
        return !root;
    }
    
    /* Hàm in danh sách liên kết theo thứ tự từ đầu tới cuối */
    void Print()
    {
        if (!isEmpty()) {
            struct sNode* current = root;
            while (current->next != NULL) {
                printf("%d ", current->data);
                current = current->next;
            }
            printf("%d ", current->data);
            printf("\n");
        }
    }


    /* Hàm in danh sách liên kết (ngăn xếp) theo thứ tự đảo ngược 
        dùng đệ quy */
    void rPrint(struct sNode* temp)
    {
        struct sNode* current = temp;
        if (current == NULL) {
            printf("\n");
            return;
        }
        rPrint(current->next);
        printf("%d ", current->data);
    }
};

int main()
{
    /* Code để kiểm tra hoạt động của chương trình,
    gọi hàm Print() hoặc rPrint() sau mỗi bước để kiểm tra tính đúng đắn */
    Stack S;
    S.Push(1); /* 1 */
    S.Push(2); /* 2, 1 */
    //S.Print();
    S.rPrint(S.root);
    S.Push(5); /* 5, 2, 1 */
    //S.Print();
    S.rPrint(S.root);
    S.Pop(); /* 2, 1 */
    //S.Print();
    S.rPrint(S.root);

    return 0;
}
