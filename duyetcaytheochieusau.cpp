#include<iostream>
using namespace std;
 
struct Node{
    int data;
    struct Node *left;
    struct Node *right;
};

void Preorder(struct Node *root)
{
    /* Ta không thể gọi đệ quy hàm mãi mãi, nên ta cần điều kiện dừng */
    /* Việc gọi đệ quy sẽ kết thúc khi cây hoặc cây con rỗng */
    /* Hay nói cách khác, việc gọi đệ quy sẽ kết thúc khi node gốc là NULL */
    if(root == NULL) return;
    /* Đầu tiên hàm sẽ thăm và in ra dữ liệu ở node gốc */
    cout << root->data << " ";
    /* Giờ ta sẽ gọi đệ quy để thăm các node của cây con bên trái */
    /* Đối số được truyền vào là địa chỉ con trái của node gốc hiện tại */
    /* Bởi vì con trái của node gốc hiện tại sẽ là node gốc của cây con trái */
    Preorder(root->left); 
    /* Tiếp theo ta gọi đệ quy để thăm các node của cây con bên phải */
    /* Đối số được truyền vào là địa chỉ con phải của node gốc hiện tại */
    /* Bởi vì con phải của node gốc hiện tại sẽ là node gốc của cây con phải*/
    Preorder(root->right);
}

void Inorder(struct Node *root)
{
    /* Ta không thể gọi đệ quy hàm mãi mãi, nên ta cần điều kiện dừng */
    /* Việc gọi đệ quy sẽ kết thúc khi cây hoặc cây con rỗng */
    /* Hay nói cách khác, việc gọi đệ quy sẽ kết thúc khi node gốc là NULL */
    if(root == NULL) return;
    /* Đầu tiên ta gọi đệ quy để thăm các con bên trái */
    /* Đối số được truyền vào là địa chỉ con trái của node gốc hiện tại */
    Inorder(root->left); 
    /* Sau đó thăm và in ra dữ liệu ở node gốc */
    cout << root->data << " ";
    /* Tiếp theo ta gọi đệ quy để thăm các con bên phải */
    /* Đối số được truyền vào là địa chỉ con phải của node gốc hiện tại */
    Inorder(root->right); 
}

void PostOrder(struct Node *root)
{
    /* Ta không thể gọi đệ quy hàm mãi mãi, nên ta cần điều kiện dừng */
    /* Việc gọi đệ quy sẽ kết thúc khi cây hoặc cây con rỗng */
    /* Hay nói cách khác, việc gọi đệ quy sẽ kết thúc khi node gốc là NULL */
    if(root == NULL) return;
    /* Đầu tiên ta gọi đệ quy để thăm các con bên trái */
    /* Đối số được truyền vào là địa chỉ con trái của node gốc hiện tại */
    PostOrder(root->left); 

    /* Tiếp theo ta gọi đệ quy để thăm các con bên phải */
    /* Đối số được truyền vào là địa chỉ con phải của node gốc hiện tại */
    PostOrder(root->right);
    /* Sau đó thăm và in ra dữ liệu ở node gốc */
    cout << root->data << " ";
}

Node* Insert(Node *root,char data) {
	if(root == NULL) {
		root = new Node();
		root->data = data;
		root->left = root->right = NULL;
	}
	else if(data <= root->data)
		root->left = Insert(root->left,data);
	else 
		root->right = Insert(root->right,data);
	return root;
}

int main() {
	/* Tạo một cây như sau
	               50 (A)
			     /        \
			  30(B)        80(C)
			 /       \        /     \
			20(D)    35(E)   60(F)    90(G)
		   /     \     /
    	 15(H)  22(I) 32(J)
    */
	Node* root = NULL;
	root = Insert(root,50);
	root = Insert(root,30); 
	root = Insert(root,80); 
	root = Insert(root,20);
	root = Insert(root,35);
	root = Insert(root,60);
	root = Insert(root,90);
	root = Insert(root,15);
	root = Insert(root,22);
	root = Insert(root,32);
	/* In ra các node theo từng cách duyệt ́*/ 
	Preorder(root);
	cout << endl;
	Inorder(root);
	cout << endl;
	PostOrder(root);
	cout << endl;
	return 0;
}
