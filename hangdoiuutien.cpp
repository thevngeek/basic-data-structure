#include<iostream>
using namespace std;

int N;

void insert_element(int A[ ], int val)
{
    N = N + 1; /* Tăng kích thước của hàng đợi lên 1
                          khi ta chèn thêm phần tử vào */
    int i = N;    /* N là biến toàn cục, không nên thay đổi giá trị của nó,
                         ta sử dụng biến tạm i ở đây, để có thể tùy ý xử lý */
    A[ i ] = val; /* Trước tiên phần tử được chèn vào vị trí cuối cùng của hàng đợi */
    while( i > 1 and A[ i/2 ] < A[ i ]) /* Nếu giá trị node cha nhỏ hơn giá trị của nó */
    {
        swap(A[ i/2 ], A[ i ]); /* Đổi chỗ hai node */
        i = i/2;                     /* Tiếp tục kiểm tra tại vị trí của node cha */
    }
}

void max_heap (int A[ ], int i)
{
        int largest;
        int left = 2*i;                   /* Vị trí của con bên trái */
        int right = 2*i +1 ;           /* Vị trí của con bên phải */
        if(left<= N and A[left] > A[i] ) /* N là số phần tử trong mảng, biến toàn cục */
              largest = left;
        else
             largest = i;
        if(right <= N and A[right] > A[largest] )
            largest = right;
        if(largest != i )
        {
            swap (A[i] , A[largest]);
            max_heap (A, largest);
        } 
}


int max_element(int A[ ])
{
    return A[1];  /* A[1] là node gốc trong max heap
                    và node gốc trong max heap là node có giá trị lớn nhất */
}

int pop_max_element (int A[ ])
{
    if(N == 0)
        {
            cout << "\tKhong the xoa phan tu, vi hang doi rong\n";
            return -1;
        }
    int max = A[1]; /* Lưu lại giá trị của phần tử lớn nhất trong hàng đợi */
    A[1] = A[N]; /* Phần tử cuối cùng sẽ được đặt vào vị trí của node gốc,
                            Điều này làm cho node gốc vi phạm các dặc tính của
                            max heap, và hàm max_heap() sẽ chạy lại từ vị trí này
                            để duy trì lại trật tự mới sau khi node có giá trị lớn nhất
                            bị xóa ra khỏi hàng đợi */
    N = N -1;
    max_heap(A, 1); /* Chạy hàm max heap tại node gốc mới */
    return max;
}

void print_element(int A[]){
    if(N <= 0) return;
    cout << "\t";
    for(int i = 1; i <= N; i++){
        cout << A[i] << " ";   
    }
    cout << endl;
}
int main(){
    N = 0;
    int A[10];
    insert_element(A,4);
    insert_element(A,8);
    insert_element(A,1);
    insert_element(A,7);
    insert_element(A,3);
    cout << endl;
    insert_element(A,6); print_element(A);
    pop_max_element(A); print_element(A);
    pop_max_element(A); print_element(A);
    pop_max_element(A); print_element(A);
    pop_max_element(A); print_element(A);
    pop_max_element(A); print_element(A);
    pop_max_element(A); print_element(A);
    pop_max_element(A); print_element(A);

    return 0;
}